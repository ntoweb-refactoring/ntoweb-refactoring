# ntoweb-refactoring


Quando un progetto con sottomoduli viene clonato usando git clone,
crea le directory che contengono sottomoduli, ma nessuno dei file al loro interno.

Eseguire i seguenti comandi: 

```
git clone https://gitlab.com/ntoweb-refactoring/ntoweb-refactoring.git
cd ntoweb-refactoring
git submodule init
git submodule update
```

oppure: 

```
git clone --recurse-submodules https://gitlab.com/ntoweb-refactoring/ntoweb-refactoring.git
```